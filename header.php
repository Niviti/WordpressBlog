<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package materialwp
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- Czcionki -->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Spectral" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Patrick+Hand+SC" rel="stylesheet">


<script type="text/javascript">

$(document).ready(function() {
$( ".main-theme" ).hover(function() {
$( ".black-box" ).css('background', '#212121');
$('.black-box').css('opacity', '0.6');   
},   
   function() {
    $('.black-box').css( "background", 'none');
    $('.black-box').css('opacity', '1');
  }
);

 function setupAnimations()
 {
    
    jQuery('.entry-meta-title-header').addClass('Opacity0 animated');
    jQuery('.first-article-title').addClass('Opacity0 animated');
    jQuery('.text-hot').addClass('Opacity0 animated');
    jQuery('.animated').addClass('Opacity0 animated');
     jQuery('.margin-top').addClass('Opacity0 animated');
      jQuery('.margin-top-B').addClass('Opacity0 animated');

 }

 function RunAnimations()
 {
  
 jQuery('.entry-meta-title-header').each(function()
 {
  jQuery(this).viewportChecker({
  classToAdd:  ' fadeIn '  ,
  offset: 200
  })
  
 }); 

 jQuery('.first-article-title').each(function()
 {
  
  jQuery(this).viewportChecker({
  classToAdd:  ' fadeIn  '  ,
  offset: 200
  })
  
 }); 

  jQuery('.text-hot').each(function()
 {
  
  jQuery(this).viewportChecker({
  classToAdd:  ' fadeIn  '  ,
  offset: 200
  })
  
 }); 

   jQuery('.animated').each(function()
 {
  
  jQuery(this).viewportChecker({
  classToAdd:  ' fadeIn  '  ,
  offset: 200
  })
  
 }); 

    jQuery('.margin-top').each(function()
 {
  
  jQuery(this).viewportChecker({
  classToAdd:  ' fadeIn  '  ,
  offset: 200
  })
  
 }); 

     jQuery('.margin-top-B').each(function()
 {
  
  jQuery(this).viewportChecker({
  classToAdd:  ' fadeIn  '  ,
  offset: 200
  })
  
 }); 

}


setupAnimations()
RunAnimations()

});

</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
   <!-- 	<a class="skip-link screen-reader-text" href="#content"> <?php /** _e( 'Skip to content', 'materialwp' ); */ ?></a>  -->

	<header id="masthead" class="site-header" role="banner">


    <div class="nav-test">  <ol class="navbar-list">     <li> <a href="/"> Główna </a></li>   <li> <a href="/idea">Zanim Zaczniesz ! </a></li> <li> <a href="/omnie">O mnie</a></li>  <li><a href="/galeria"> Galeria </a></li></a>  <li><a href="/kontakt"> Kontakt</a></li>  </ol> </div>

	<div class="logo">   </div>

	
	</header><!-- #masthead -->

	<div id="content" class="site-content">
