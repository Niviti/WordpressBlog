<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package materialwp
 */

get_header(); ?>

<div class="container">
	<div class="row">

	
			<main id="main" class="site-main" role="main">

				<?php if ( have_posts() ) : ?>

					<header>
						<?php

                            $title= get_the_archive_title();
                            $new=str_replace('Miesiąc:', ' ',$title);
                            $new2=str_replace('Tag:', ' ',$new);
                         

                       ?><div class="title-Archivie"> <?php    echo $new2;        ?>   </div> <?
						
						
						?>
					</header><!-- .page-header -->

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );
						?>

					<?php endwhile; ?>

					<?php materialwp_paging_nav(); ?>

				<?php else : ?>

					<?php get_template_part( 'content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->
	

	</div> <!-- .row -->
</div> <!-- .container -->

<?php get_footer(); ?>
