<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package materialwp
 */

get_header(); ?>

<div class="container">
	<div class="row">

	<div id="primary" class="col-md-12 col-lg-12">
		<main id="main" class="site-main" role="main">

		
			

					
						<div class="page-content">
					             
					          <div class="ERROR-404">   ERROR-404 Page not found !  </div>

						</div><!-- .page-content -->

			
			
		

		</main><!-- #main -->
	</div><!-- #primary -->

	</div> <!-- .row -->
</div> <!-- .container -->

<?php get_footer(); ?>
