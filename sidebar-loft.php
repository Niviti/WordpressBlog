<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package materialwp
 */

if ( ! is_active_sidebar( 'sidebar-loft' ) ) {
	return;
}
?>


		<?php dynamic_sidebar( 'sidebar-loft' ); ?>
	



