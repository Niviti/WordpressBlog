<?php
/**
 * The template for displaying all single posts.
 *
 * @package materialwp
 */

get_header(); ?>



			<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>

			
			<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->
		<!-- #primary -->



<?php get_footer(); ?>
